package vidstreamer.thenotoriousrog.vidstreamer.presenters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import vidstreamer.thenotoriousrog.vidstreamer.RecyclerCardViewAdapter;
import vidstreamer.thenotoriousrog.vidstreamer.interfaces.Updater;

/*
    * The Presenter of the MainActivity that handles al of the click events that are needed for the rest of the activity.
 */
public class MainActivityPresenter {

    private RecyclerCardViewAdapter adapter;
    private RecyclerView recyclerView;
    private Updater updater;
    private Context context;

    public MainActivityPresenter(RecyclerCardViewAdapter adapter, RecyclerView recyclerView, Updater updater, Context context) {

        this.adapter = adapter;
        this.recyclerView = recyclerView;
        this.updater = updater;
        this.context = context;

        startPresenting();
    }

    // begins creating a populating the array once the adapter has been created and properly set.
    private void startPresenting() {
        updater.showProgressBar();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        updater.update(recyclerView, adapter);
        updater.hideProgressBar();
    }

}
