package vidstreamer.thenotoriousrog.vidstreamer.presenters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.VideoView;

import vidstreamer.thenotoriousrog.vidstreamer.R;
import vidstreamer.thenotoriousrog.vidstreamer.backend.VidRepo;

public class PlayVideoActivityPresenter {

    // interface used to update the PlayVideoActivity.
    public interface Updater {
        void showProgressBar();
        void hideProgressBar();
        void allowSeeking(int maxDuration, String duration); // essentially sets the seek bar to work properly for the user to seek the seekbar.
        void restartState(String newVideoURL, int newVidPos); // removes old state saved in activity.
        void showErrorAlert();
        void kill(); // used to kill the activity if there is an error processing the video.
        void step(int currPos, String watched); // step the seekbar and update the watch counter.
    }

    private String[] vidLinks = VidRepo.getRawLinks();
    private boolean isPaused = false; // tells if the system is paused or not.
    private boolean showingMediaControls = false; // currently not showing media controls.
    private String videoUrl;
    private VideoView videoView;
    private RelativeLayout mediaControls;
    private ImageView playButton;
    private ImageView pauseButton;
    private Updater updater;
    private int vidPos;
    private int currVidPlace = 0;
    private boolean isStepping = false; // used to prevent multiple handlers running the same task
    private Handler handler = new Handler(); // used to step through our stepper

    // finishes the activity ONLY upon a processing error of a video
    private Runnable activityKiller = new Runnable() {
        @Override
        public void run() {
            updater.kill();
        }
    };

    // in charge of creating the stepper for the seekbar.
    private Runnable stepper = new Runnable() {
        @Override
        public void run() {
            String time = getRealTime(videoView.getCurrentPosition());
            updater.step(videoView.getCurrentPosition()/1000, time);
            handler.postDelayed(this, 1000); // restart the handler to post every second.
        }
    };

    public PlayVideoActivityPresenter(final String videoUrl, int vidPos, final VideoView videoView, RelativeLayout mediaControls, final Updater updater,
                                      int currVidPlace, SeekBar seekBar) {
        this.videoUrl = videoUrl;
        this.vidPos = vidPos;
        this.videoView = videoView;
        this.mediaControls = mediaControls;
        playButton = mediaControls.findViewById(R.id.playButton);
        pauseButton = mediaControls.findViewById(R.id.pauseButton);
        this.updater = updater;
        this.currVidPlace = currVidPlace;

        updater.showProgressBar();
        videoView.setVideoURI(Uri.parse(videoUrl));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                PlayVideoActivityPresenter.this.videoView.seekTo(PlayVideoActivityPresenter.this.currVidPlace);
                PlayVideoActivityPresenter.this.videoView.start();
                PlayVideoActivityPresenter.this.updater.hideProgressBar();
                PlayVideoActivityPresenter.this.updater.allowSeeking(videoView.getDuration()/1000, getRealTime(videoView.getDuration()));

                if(!isStepping) {
                    handler.postDelayed(stepper, 1000);
                    isStepping = true;
                }
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                PlayVideoActivityPresenter.this.currVidPlace = 0; // restart the place of the video.
                updater.restartState(PlayVideoActivityPresenter.this.videoUrl, PlayVideoActivityPresenter.this.vidPos);
                skipClicked(); // link to the next video seamlessly
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                updater.showErrorAlert();
                PlayVideoActivityPresenter.this.currVidPlace = 0; // reset the current place of the video to be 0 for when another video is loaded.
                new Handler().postDelayed(activityKiller, 3000); // kill the activity after 3 seconds.
                return true;
            }
        });
    }

    private String getRealTime(int milli) {
        int min = (milli/1000) / 60;
        int sec = (milli/1000) % 60;
        String time;
        if(sec < 10) {
            time = min + ":0" + sec;
        } else {
            time = min + ":" + sec;
        }

        return time;
    }

    public void pauseClicked() {
        videoView.pause();
        currVidPlace = videoView.getCurrentPosition();
        isPaused = true; // we are now paused.

        playButton.setVisibility(View.VISIBLE);
        pauseButton.setVisibility(View.GONE);
    }

    public void playClicked() {

        updater.showProgressBar();
        videoView.stopPlayback();
        videoView.setVideoURI(Uri.parse(videoUrl));

        isPaused = false;
        pauseButton.setVisibility(View.VISIBLE);
        playButton.setVisibility(View.GONE);
    }

    // goes to the next video in the list.
    public void skipClicked() {

        // prepare seekbar and vidview for a new video to load.
        handler.removeCallbacks(stepper);
        isStepping = false;
        updater.showProgressBar(); // begin loading again.
        this.currVidPlace = 0;

        if(vidPos == vidLinks.length-1) {
            this.videoUrl = vidLinks[0];
            this.vidPos = 0;
            videoView.setVideoPath(videoUrl); // play the first video.
        } else {
            this.videoUrl = vidLinks[vidPos+1];
            this.vidPos++;
            videoView.setVideoPath(videoUrl); // play the next video below the list very important.
        }

        updater.restartState(videoUrl, vidPos);
    }

    public void prevClicked() {

        // prepare seekbar and vidview for a new video to load.
        handler.removeCallbacks(stepper);
        isStepping = false;
        updater.showProgressBar(); // begin loading again.
        this.currVidPlace = 0;

        if(vidPos == 0) {
            this.videoUrl = vidLinks[vidLinks.length-1];
            this.vidPos = vidLinks.length-1;
            videoView.setVideoPath(videoUrl); // play last video.
        } else {
            this.videoUrl = vidLinks[vidPos-1];
            this.vidPos--;
            videoView.setVideoPath(videoUrl); // play prev video.
        }

        updater.restartState(videoUrl, vidPos);
    }

    public void showHideMediaControls() {
        if(showingMediaControls) {
            mediaControls.animate()
                    .alpha(0.0f)
                    .setDuration(200)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mediaControls.setVisibility(View.GONE);
                        }
                    });
            showingMediaControls = false;
        } else {

            mediaControls.setVisibility(View.VISIBLE);
            mediaControls.setAlpha(0.0f);
            mediaControls.animate()
                    .alpha(1.0f)
                    .setDuration(200)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            //mediaControls.setVisibility(View.VISIBLE);
                        }
                    });

            showingMediaControls = true;
        }
    }

    // save the currently seeked location and essentially reload the video to that location.
    public void userSeekedTo(int seekLocation) {
        updater.showProgressBar();
        currVidPlace = seekLocation;
        handler.removeCallbacks(stepper);
        isStepping = false; // tell the presenter that we are no longer stepping.
        videoView.stopPlayback();
        videoView.setVideoPath(videoUrl);
    }

    public int getCurrVidPlace() {
        return videoView.getCurrentPosition();
    }

}
