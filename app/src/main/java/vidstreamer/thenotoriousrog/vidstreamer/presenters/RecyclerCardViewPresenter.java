package vidstreamer.thenotoriousrog.vidstreamer.presenters;

import vidstreamer.thenotoriousrog.vidstreamer.interfaces.VideoCardView;

public class RecyclerCardViewPresenter {

    private final String[] vidLinks;

    public RecyclerCardViewPresenter(String[] vidLinks) {
        this.vidLinks = vidLinks;
    }

    public void onBindVideoCardViewAtPosition(int pos, VideoCardView cardView) {
        cardView.showProgressBar(); // beginning to have the app load the video showing a progress bar in meantime.
        String url = vidLinks[pos];
        cardView.setTitle("Video " + (pos + 1));
        cardView.setDuration("Loading");
        cardView.setVideoSrc(url);
        cardView.setClickListener();
    }

    public int getCardViewCount() {
        return vidLinks.length;
    }

}
