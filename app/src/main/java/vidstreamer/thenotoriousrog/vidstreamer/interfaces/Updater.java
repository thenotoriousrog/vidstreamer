package vidstreamer.thenotoriousrog.vidstreamer.interfaces;

import android.support.v7.widget.RecyclerView;

import vidstreamer.thenotoriousrog.vidstreamer.RecyclerCardViewAdapter;

// This interface is used to load items on the main ui from the MainActivity.
public interface Updater {
    void update(RecyclerView recyclerView, RecyclerCardViewAdapter adapter);
    void showProgressBar();
    void hideProgressBar();
}