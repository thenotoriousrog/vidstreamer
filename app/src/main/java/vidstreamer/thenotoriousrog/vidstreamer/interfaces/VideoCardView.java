package vidstreamer.thenotoriousrog.vidstreamer.interfaces;

/*
    * An interface for specific items within the RecyclerViewPresenter
 */
public interface VideoCardView {

    void setTitle(String title);
    void setVideoSrc(String url);
    void setDuration(String duration);
    void setClickListener();
    void showProgressBar();
    void hideProgressBar();

}
