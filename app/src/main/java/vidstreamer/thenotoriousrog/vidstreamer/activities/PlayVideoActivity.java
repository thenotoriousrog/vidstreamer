package vidstreamer.thenotoriousrog.vidstreamer.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import vidstreamer.thenotoriousrog.vidstreamer.R;
import vidstreamer.thenotoriousrog.vidstreamer.presenters.PlayVideoActivityPresenter;

public class PlayVideoActivity extends AppCompatActivity implements View.OnClickListener, PlayVideoActivityPresenter.Updater {

    private RelativeLayout mediaControls;
    private RelativeLayout playVideoLayout;
    private ImageView playButton;
    private ImageView pauseButton;
    private ImageView skipButton;
    private ImageView prevButton;
    private VideoView videoView;
    private String videoURL;
    private int vidPos;
    private PlayVideoActivityPresenter presenter;
    private ProgressBar progressBar;
    private SharedPreferences sharedPreferences;
    private SeekBar seekBar;
    private TextView currTimeWatchedText; // the amount of time that the user spent watching this video.
    private TextView durationText; // the duration of text for the video.

    private void setButtons() {
        this.videoView = findViewById(R.id.vidView);
        this.mediaControls = findViewById(R.id.mediaControls);
        this.playButton = findViewById(R.id.playButton);
        this.pauseButton = findViewById(R.id.pauseButton);
        this.skipButton = findViewById(R.id.skipButton);
        this.prevButton = findViewById(R.id.prevButton);
    }

    private void setClickListeners() {
        videoView.setOnClickListener(this);
        mediaControls.setOnClickListener(this);
        playButton.setOnClickListener(this);
        pauseButton.setOnClickListener(this);
        skipButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video);

        playVideoLayout = findViewById(R.id.PlayVideoLayout);
        seekBar = findViewById(R.id.seekbar);
        currTimeWatchedText = findViewById(R.id.currTimeWatched);
        durationText = findViewById(R.id.duration);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Bundle args = getIntent().getBundleExtra("args");

        boolean vidSelected = sharedPreferences.getBoolean("Selected", true); // default to true to use the activity's passed in information.
        if(vidSelected) { // use the information passed in by the user.
            this.videoURL = args.getString("VideoURL");
            this.vidPos = args.getInt("VidPos");

        } else { // if not directly selected use the information passed in from the user.
            this.videoURL = sharedPreferences.getString("VideoURL", videoURL); // default is the passed in video url
            this.vidPos = sharedPreferences.getInt("VidPos", vidPos); // default is the video pos passed in.
        }

        int currVidTime = sharedPreferences.getInt("CurrVidTime", 0); // get the current videos time before loading the presenter.

        setButtons();
        setClickListeners();
        this.progressBar = findViewById(R.id.progressBar);
        this.presenter = new PlayVideoActivityPresenter(videoURL, vidPos, videoView, mediaControls, this, currVidTime, seekBar);
    }

    @Override
    public void onClick(View view) {

        if(view.getId() == videoView.getId() || view.getId() == mediaControls.getId()) {
            System.out.println("triggering the media controls!!!");
            presenter.showHideMediaControls();
        } else if(view.getId() == pauseButton.getId()) {
            presenter.pauseClicked();
        } else if(view.getId() == playButton.getId()) {
            presenter.playClicked();
        } else if(view.getId() == skipButton.getId()) {
            presenter.skipClicked();
        } else if(view.getId() == prevButton.getId()) {
            presenter.prevClicked();
        }

    }

    @Override
    public void onPause() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("CurrVidTime", presenter.getCurrVidPlace());
        editor.apply(); // note: if the background thread that saves these preferences is not finished the preferences may not be saved which will be a big deal and something we will have to fix.
        super.onPause();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private String getRealTime(int milli) {
        int min = (milli/1000) / 60;
        int sec = (milli/1000) % 60;
        String time;
        if(sec < 10) {
            time = min + ":0" + sec;
        } else {
            time = min + ":" + sec;
        }

        return time;
    }

    // Allow for the seekbar to work properly.
    // also update the max text for the video
    @Override
    public void allowSeeking(int maxDuration, String duration) {
        seekBar.setMax(maxDuration);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    seekBar.setProgress(progress);
                    currTimeWatchedText.setText(getRealTime(progress*1000));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            // Note: since we are loading the video from the web, we only want to restart the video once the user releases the seekbar.
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                presenter.userSeekedTo(seekBar.getProgress()*1000);
            }
        });

        durationText.setText(duration);
    }

    @Override
    public void restartState(String newVideoURL, int newVidPos) {
        this.videoURL = newVideoURL;
        this.vidPos = newVidPos;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("CurrVidTime", 0); // reset the time to be 0 to restart it properly.
        editor.putString("VideoURL", newVideoURL);
        editor.putInt("VidPos", newVidPos);
        editor.putBoolean("Selected", false); // the user did not select this video, thus telling the system to use the shared prefs video information.
        editor.apply(); // note: if the background thread that saves these preferences is not finished the preferences may not be saved which will be a big deal and something we will have to fix.
    }

    @Override
    public void showErrorAlert() {
        Snackbar.make(playVideoLayout, getString(R.string.VidProcessingError), Snackbar.LENGTH_INDEFINITE).show();
    }

    // kills the activity after the user
    @Override
    public void kill() {
        finish();
    }

    @Override
    public void step(int currPos, String dur) {
        seekBar.setProgress(currPos);
        currTimeWatchedText.setText(dur);
    }
}
