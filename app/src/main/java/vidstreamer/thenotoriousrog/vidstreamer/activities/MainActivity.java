package vidstreamer.thenotoriousrog.vidstreamer.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import vidstreamer.thenotoriousrog.vidstreamer.R;
import vidstreamer.thenotoriousrog.vidstreamer.RecyclerCardViewAdapter;
import vidstreamer.thenotoriousrog.vidstreamer.interfaces.Updater;
import vidstreamer.thenotoriousrog.vidstreamer.backend.Utils;
import vidstreamer.thenotoriousrog.vidstreamer.backend.VidRepo;
import vidstreamer.thenotoriousrog.vidstreamer.presenters.MainActivityPresenter;
import vidstreamer.thenotoriousrog.vidstreamer.presenters.RecyclerCardViewPresenter;

public class MainActivity extends AppCompatActivity implements Updater {

    private MainActivityPresenter presenter;
    private RecyclerCardViewAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Utils.setMainActivity(this);

        progressBar = findViewById(R.id.progressBar);
        this.adapter = new RecyclerCardViewAdapter(new RecyclerCardViewPresenter(VidRepo.getRawLinks()));
        this.recyclerView = findViewById(R.id.mainRecyclerView);
        this.presenter = new MainActivityPresenter(adapter, recyclerView, this, this); // use this twice as we can send an updater and a context.
    }

    @Override
    public void update(RecyclerView recyclerView, RecyclerCardViewAdapter adapter) {
        adapter.notifyDataSetChanged();
        recyclerView.invalidate();
        System.out.println("Updating the UI right now!");
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }
}
