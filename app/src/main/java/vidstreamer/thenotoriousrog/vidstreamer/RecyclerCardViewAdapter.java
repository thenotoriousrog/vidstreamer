package vidstreamer.thenotoriousrog.vidstreamer;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import vidstreamer.thenotoriousrog.vidstreamer.presenters.RecyclerCardViewPresenter;

public class RecyclerCardViewAdapter extends RecyclerView.Adapter<RecyclerCardViewHolder> {

    private final RecyclerCardViewPresenter presenter;

    public RecyclerCardViewAdapter(RecyclerCardViewPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public RecyclerCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerCardViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.video_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerCardViewHolder recyclerCardViewHolder, int i) {
        presenter.onBindVideoCardViewAtPosition(i, recyclerCardViewHolder);
    }

    @Override
    public int getItemCount() {
        return presenter.getCardViewCount();
    }
}
