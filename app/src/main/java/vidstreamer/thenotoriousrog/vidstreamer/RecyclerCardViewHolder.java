package vidstreamer.thenotoriousrog.vidstreamer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import vidstreamer.thenotoriousrog.vidstreamer.activities.PlayVideoActivity;
import vidstreamer.thenotoriousrog.vidstreamer.interfaces.VideoCardView;
import vidstreamer.thenotoriousrog.vidstreamer.backend.Utils;
import vidstreamer.thenotoriousrog.vidstreamer.backend.VidRepo;

public class RecyclerCardViewHolder extends RecyclerView.ViewHolder implements VideoCardView {

    private View cardView;
    private TextView vidTitleTextView;
    private VideoView vidView;
    private TextView vidDurationTextView;
    private RelativeLayout miniMediaControls;
    private String videoURL;
    private boolean loopingVideo = false;
    private ProgressBar progressBar;
    private Handler handler = new Handler(); // strictly used for replaying the video after a few seconds.

    Runnable VidLooper = new Runnable() {
        @Override
        public void run() {
            vidView.stopPlayback();
            vidView.setVideoPath(videoURL);
            handler.postDelayed(this, 5000); // loop this handler every 5 seconds.
        }
    };

    public RecyclerCardViewHolder(View cardView) {
        super(cardView);
        this.cardView = cardView;
        progressBar = cardView.findViewById(R.id.progressBar);
        vidTitleTextView = cardView.findViewById(R.id.VidTitle);
        vidView = cardView.findViewById(R.id.video);

        vidView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {

                mediaPlayer.setVolume(0f, 0f); // remove sound from the video.
                vidView.start();

                if(!loopingVideo) { // make sure we aren't looping in order to prevent the handler from being created multiple times when scrolling through the videos.
                    handler.postDelayed(VidLooper, 5000); // restart the video every 5 seconds. The user can only watch the full video by clicking on it.
                    loopingVideo = true;
                }

                RecyclerCardViewHolder.this.hideProgressBar(); // have the progress bar hide from the rest of the app without having to hide the progress bar.

                // calculate the real duration of this video
                int rawTime = vidView.getDuration();
                int minutes = (rawTime/1000) / 60;
                int seconds = (rawTime/1000) % 60;
                String realDuration = minutes + ":" + seconds;
                RecyclerCardViewHolder.this.setDuration(realDuration);
            }
        });

        vidDurationTextView = cardView.findViewById(R.id.vidDuration);
        //miniMediaControls = cardView.findViewById(R.id.miniMediaControls);
    }

    @Override
    public void setTitle(String title) {
        vidTitleTextView.setText(title);
    }

    @Override
    public void setVideoSrc(String url) {
        vidView.setVideoPath(url);
        this.videoURL = url;
    }

    @Override
    public void setDuration(String duration) {
        vidDurationTextView.setText(duration);
    }

    @Override
    public void setClickListener() {
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Utils.getMainActivity().getApplicationContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("Selected", true); // tell the system that the user clicked this video to play only.
                editor.putInt("CurrVidTime", 0); // ensure that the video starts at the beginning
                editor.commit(); // force the pref to be saved immediately.
                Intent playVideoActivityIntent = new Intent(view.getContext(), PlayVideoActivity.class);
                Bundle args = new Bundle();
                args.putString("VideoURL", videoURL);
                args.putInt("VidPos", VidRepo.getURLPos(videoURL));
                playVideoActivityIntent.putExtra("args", args);

                view.getContext().startActivity(playVideoActivityIntent);
            }
        });
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    // Overriding this method to attempt to remove the handler upon destruction of this class.
    @Override
    public void finalize() {
        handler.removeCallbacks(VidLooper); // remove the callbacks for the handler to not leak actions further slowing down the app.
    }
}
