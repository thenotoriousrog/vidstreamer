package vidstreamer.thenotoriousrog.vidstreamer.backend;

import vidstreamer.thenotoriousrog.vidstreamer.activities.MainActivity;

/*
    * A simple class with Global items and properties to be used by anywhere.
 */
public class Utils {

    private static MainActivity mainActivity;

    public static void setMainActivity(MainActivity main) {
        mainActivity = main;
    }

    public static MainActivity getMainActivity() {
        return mainActivity;
    }
}
