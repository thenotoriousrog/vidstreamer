package vidstreamer.thenotoriousrog.vidstreamer.backend;

import android.content.Context;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/*
    * Simply holds a list of videos to be grabbed anywhere in the application.
 */
public class VidRepo {

    private static String[] vidLinks =  {
            "https://s3.amazonaws.com/interview-quiz-stuff/tos-trailer/master.m3u8",
            "https://s3.amazonaws.com/interview-quiz-stuff/tos/master.m3u8"
    };

    public static String[] getRawLinks() {
        return vidLinks;
    }

    public static int getURLPos(String url) {

        for(int i = 0; i < vidLinks.length; i++) {
            if(vidLinks[i].equals(url)) {
                return i;
            }
        }
        return -1; // url is not in our list.
    }
}
