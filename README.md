### VidStreamer Demo Application ###

This application took a total of roughly 26.5 hours to complete spread across the weekend. I wrote issues as well as milestones to accompany the tasks that were needed to build the application as close to the original architecture design I created as possible. To view these issues simply click on issues > all. When I resolved the issue I wrote a comment detailing how long each issue took to complete.

The app was built to follow the MVP pattern as best as I could. I chose to build the app without an appbar simply for maximizing screen space for the videos. I also chose to go with a RecyclerView instead of the ListView as I wanted to build the app to follow a more modern approach since we are working with videos. A RecyclerView would be the element I would use in this case to load up the videos and play their short snippets without sacrificing scrolling speed. While this app only made use of two videos, theoretically this app could hold as many video links we wanted and it would be able to handle it.


## How to install/use ##

You may browse the code and under app > src > main > java > APKs you can find a copy of the VidStreamer.apk that you can install on your phone to try out. Please make sure to allow for Unknown Sources to be checked on your phone so you can install it. The app does not track or hold any data.

## Known Issues ##

While the app is code-complete and fully-functional, if there are more videos that require the user to scroll, the RecyclerView holds a VideoView and sometimes the link that is used to load the video encounters an error while connecting to the internet. To fix this, I would simply implement the onErrorListener for the VideoView and if an error occurred I would simply send in the link (while also making sure that the loop handler was reset). This should remove that annoying dialog and make the RecyclerView that much smoother when scrolling.